
from flask import Flask, render_template, Response, request
from camera import VideoCamera
from werkzeug.utils import redirect


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/video_feed')
def video_feed():

    return Response(gen(VideoCamera()), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/upload_file', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']

        if file.filename == '':
            return redirect(request.url)

        if request.form['type'] == 'image':
            filename = "base.jpg"
        elif request.form['type'] == 'video':
            filename = "base.mp4"
        else:
            filename = "temp"

        file.save(filename)

        return 'ok'

if __name__ == '__main__':
    f_cam = False
    app.run(host='0.0.0.0', debug=True)
