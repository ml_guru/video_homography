import cv2
import numpy as np
import main_backend


class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)

        # img1 = cv2.imread('2.png', 0)  # queryImage
        img1 = cv2.imread('base.jpg', 0)
        video_name = 'base.mp4'

        # ------------------- load video data ---------------------
        print "loading video ..."
        cap = cv2.VideoCapture(video_name)
        self.img_video = []
        self.video_frm_cnt = 0
        while True:
            ret, img = cap.read()
            if not ret:
                break
            self.img_video_h, self.img_video_w = img.shape[:2]
            self.video_frm_cnt += 1
            self.img_video.append(img)

        # -------------------- init process --------------------------
        print " main process"

        # Initiate SIFT detector
        self.sift = cv2.xfeatures2d.SIFT_create()
        index_params = dict(algorithm=0, trees=5)
        search_params = dict(checks=50)
        self.flann = cv2.FlannBasedMatcher(index_params, search_params)

        self.kp1, self.des1 = self.sift.detectAndCompute(img1, None)
        self.sh, self.sw = img1.shape

        self.frm_cnt = 0
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.

        img_height, img_width = image.shape[:2]
        self.frm_cnt += 1

        match_region = main_backend.get_match_region(image, self.sift, self.flann, self.des1, self.kp1, self.sw, self.sh)

        image = cv2.polylines(image, [match_region], True, 255, 2, cv2.LINE_AA)
        video_frame = self.img_video[self.frm_cnt % self.video_frm_cnt]

        if match_region is not None:
            pts1 = np.float32([[0, 0], [0, self.img_video_h], [self.img_video_w, self.img_video_h],
                               [self.img_video_w, 0]])
            pts2 = np.float32([match_region[0][0], match_region[1][0], match_region[2][0], match_region[3][0]])
            M = cv2.getPerspectiveTransform(pts1, pts2)
            dst = cv2.warpPerspective(video_frame, M, (img_width, img_height))

            dst_gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
            dst_bin = cv2.threshold(dst_gray, 1, 1, cv2.THRESH_BINARY)[1]
            dst_bin = cv2.cvtColor(dst_bin, cv2.COLOR_GRAY2BGR)

            image = image * (1 - dst_bin) + dst * dst_bin

        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
