import numpy as np
import cv2
import time


def get_match_region(img_big, sift, flann, des1, kp1, sw, sh):

    # find the keypoints and descriptors with SIFT
    kp2, des2 = sift.detectAndCompute(img_big, None)

    try:
        matches = flann.knnMatch(des1, des2, k=2)

        # store all the good matches as per Lowe's ratio test.
        good = []
        total_match = 0
        for m, n in matches:
            total_match += 1
            if m.distance < 0.7*n.distance:
                good.append(m)

        # print len(good), total_match
        if len(good) > total_match*0.027+6:
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            map_func, _ = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
            pts = np.float32([[0, 0], [0, sh - 1], [sw - 1, sh - 1], [sw - 1, 0]]).reshape(-1, 1, 2)
            dst = np.int32(cv2.perspectiveTransform(pts, map_func))

        else:
            # print "Not enough matches are found."
            dst = None

    except:
        dst = None

    return dst


if __name__ == '__main__':

    # img1 = cv2.imread('r1.jpg', 0)        # queryImage
    img1 = cv2.imread('base.jpg', 0)  # queryImage
    # img2 = cv2.imread('6.jpg')        # trainImage
    video_name = 'base.mp4'

    # ------------------- load video data ---------------------
    print "loading video ..."
    cap = cv2.VideoCapture(video_name)
    img_video = []
    video_frm_cnt = 0
    while True:
        ret, img = cap.read()
        if not ret:
            break
        img_video_h, img_video_w = img.shape[:2]
        video_frm_cnt += 1
        img_video.append(img)

    # -------------------- init process --------------------------
    print " main process"
    FLANN_INDEX_KDTREE = 0

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    kp1, des1 = sift.detectAndCompute(img1, None)
    sh, sw = img1.shape

    # ----------------------- loop ---------------------------------
    cap = cv2.VideoCapture(0)

    frm_cnt = 0
    while True:
        ret, img = cap.read()
        if not ret:
            break

        img_height, img_width = img.shape[:2]
        frm_cnt += 1

        if frm_cnt == 1:
            # fource = cv2.VideoWriter_fourcc(*"XVID")
            fource = cv2.VideoWriter_fourcc(*"MJPG")
            video_writer = cv2.VideoWriter('out.avi', fource, 10.0, (img_width, img_height))

        time1 = time.time()
        match_region = get_match_region(img, sift, flann, des1, kp1, sw, sh)

        img = cv2.polylines(img, [match_region], True, 255, 2, cv2.LINE_AA)
        video_frame = img_video[frm_cnt % video_frm_cnt]
        # img[100:300, 100:300] = cv2.resize(video_frame, (200, 200), cv2.INTER_AREA)

        if match_region is not None:
            pts1 = np.float32([[0, 0], [0, img_video_h], [img_video_w, img_video_h], [img_video_w, 0]])
            pts2 = np.float32([match_region[0][0], match_region[1][0], match_region[2][0], match_region[3][0]])
            M = cv2.getPerspectiveTransform(pts1, pts2)
            dst = cv2.warpPerspective(video_frame, M, (img_width, img_height))

            dst_gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
            dst_bin = cv2.threshold(dst_gray, 1, 1,  cv2.THRESH_BINARY)[1]
            dst_bin = cv2.cvtColor(dst_bin, cv2.COLOR_GRAY2BGR)

            img = img * (1 - dst_bin) + dst * dst_bin

        cv2.imshow('gray', img)
        video_writer.write(img)

        # print time.time() - time1

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break

    video_writer.release()
    cap.release()
